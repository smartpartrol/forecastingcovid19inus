# Forecasting COVID19

Using the Forecast package in R to run a pipeline of time series models at the US county-level FIPs to predict COVID19 cumulative growth by US county to predict where cases are going to rise.

Data is from [here](https://github.com/datasets/covid-19) but sourced directly from the [Johns Hopkins University Center for Systems Science and Engineering (CSSE)](https://systems.jhu.edu/)